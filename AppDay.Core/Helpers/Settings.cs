// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;

namespace AppDay.Core.Helpers
{
    
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get { return CrossSettings.Current; }
        }

        #region Setting Constants

        private const string DEVICE_ID_KEY = "deviceId";

        #endregion


        public static string DeviceId
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(DEVICE_ID_KEY, null) ?? (DeviceId = Guid.NewGuid().ToString());
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(DEVICE_ID_KEY, value);
            }
        }

    }
}