using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using System;
using System.Windows.Input;
using MvvmCross.Platform;
using AppDay.API.Services;
using Acr.UserDialogs;
using AppDay.API.Models;
using System.Linq;

namespace AppDay.Core.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
        private Ticket _ticket;

        private ICommand _applyCommand;
        public ICommand ApplyCommand
        {
            get
            {
                return _applyCommand ?? (_applyCommand = new MvxCommand(OnApplyExecute));
            }
        }

        private ICommand _checkCommand;
        public ICommand CheckCommand
        {
            get
            {
                return _checkCommand ?? (_checkCommand = new MvxCommand(OnCheckExecute));
            }
        }

        private string _fromStation;
        public string FromStation
        {
            get
            {
                return _fromStation;
            }
            set
            {
                _fromStation = value;
                RaisePropertyChanged(() => FromStation);
            }
        }

        private string _toStation;
        public string ToStation
        {
            get
            {
                return _toStation;
            }
            set
            {
                _toStation = value;
                RaisePropertyChanged(() => ToStation);
            }
        }

        private DateTime _dateTime;
        public DateTime DateTime
        {
            get
            {
                return _dateTime;
            }
            set
            {
                _dateTime = value.AddHours(2); //HACK
                RaisePropertyChanged(() => DateTime);

                _dateTimeString = _dateTime.ToString("dd.MM.yy HH:mm");
                RaisePropertyChanged(() => DateTimeString);
            }
        }

        private string _dateTimeString;
        public string DateTimeString
        {
            get
            {
                return _dateTimeString;
            }
            set
            {
                _dateTime = DateTime.Parse(value);
            }
        }

        public MainViewModel()
        {
            FromStation = "Rostock";
            ToStation = "Hamburg";
            DateTime = DateTime.Today;
        }
        
        public override Task Initialize()
        {
            return base.Initialize();
        }

        private bool Validate()
        {
            var error = "";
            //if (DateTime < DateTime.Now)
            //    error += "Wrong date and time! It should be more than now.\n";
            if (string.IsNullOrEmpty(FromStation))
                error += "Yor forgot about the departure station.\n";
            if (string.IsNullOrEmpty(ToStation))
                error += "Yor forgot about the arrival station.\n";

            if (string.IsNullOrEmpty(error))
                return true;
            
            UserDialogs.Instance.Alert(error);
            return false;
        }

        private void OnApplyExecute()
        {
            if (!Validate())
                return;
            
            Task.Run(async () => 
            {
                var tickets = await Mvx.Resolve<ITicketsService>().GetTickets(FromStation, ToStation, DateTime);
                _ticket = tickets.FirstOrDefault();
                InvokeOnMainThread(() => {
                    if (_ticket == null)
                    {
                        UserDialogs.Instance.Alert("Sorry, nothing found\n¯\\_(ツ)_/¯");
                    }
                    else
                    {
                        UserDialogs.Instance.Confirm(new ConfirmConfig()
                        {
                            Title = "We found the best option for you",
                            Message = $"\nTime: {_ticket.Time}\nPrice: {_ticket.Price} €",
                            OkText = "Save",
                            OnAction = (confirm) => 
                            {
                                if (confirm)
                                {
                                    //TODO
                                }
                            }
                        });
                    }
                });
            });
        }

        private void OnCheckExecute()
        {
            var count = 1;
            UserDialogs.Instance.Alert($"We found {count} passenger, who have a trip in the same time with you.\n\n" +
                                       $"Direction: {_ticket.From} - {_ticket.To}\n" +
                                       $"Time: {_ticket.Time}", "Congratulations");
        }
    }
}