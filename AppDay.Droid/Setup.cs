using Android.Content;
using MvvmCross.Droid.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.Platform;
using Xablu.WebApiClient;
using AppDay.API;
using Xamarin.Android.Net;

namespace AppDay.Core.Droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new Core.App();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override void InitializeFirstChance()
        {
            Mvx.RegisterSingleton<IWebApiClient>(new WebApiClient(ApiSettings.MobileUrl, () => new AndroidClientHandler()));

            base.InitializeFirstChance();
        }
    }
}
