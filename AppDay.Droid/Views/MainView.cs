using Acr.UserDialogs;
using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;

namespace AppDay.Core.Droid.Views
{
    [Activity(Label = "AppDay", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            UserDialogs.Init(this);
            SetContentView(Resource.Layout.MainView);
        }
    }
}
