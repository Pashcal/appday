using MvvmCross.Platform.Plugins;

namespace AppDay.Core.iOS.Bootstrap
{
    public class JsonPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Json.PluginLoader>
    {
    }
}