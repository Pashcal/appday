using MvvmCross.Platform.Plugins;

namespace AppDay.Core.iOS.Bootstrap
{
    public class DownloadCachePluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.DownloadCache.PluginLoader, MvvmCross.Plugins.DownloadCache.iOS.Plugin>
    {
    }
}