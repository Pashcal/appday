// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace AppDay.Core.iOS.Views
{
	[Register ("MainViewController")]
	partial class MainViewController
	{
		[Outlet]
		UIKit.UIButton _applyButton { get; set; }

		[Outlet]
		UIKit.UIButton _checkButton { get; set; }

		[Outlet]
		UIKit.UITextField _dateTimeField { get; set; }

		[Outlet]
		UIKit.UITextField _fromTextField { get; set; }

		[Outlet]
		UIKit.UITextField _toTextField { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (_checkButton != null) {
				_checkButton.Dispose ();
				_checkButton = null;
			}

			if (_applyButton != null) {
				_applyButton.Dispose ();
				_applyButton = null;
			}

			if (_dateTimeField != null) {
				_dateTimeField.Dispose ();
				_dateTimeField = null;
			}

			if (_fromTextField != null) {
				_fromTextField.Dispose ();
				_fromTextField = null;
			}

			if (_toTextField != null) {
				_toTextField.Dispose ();
				_toTextField = null;
			}
		}
	}
}
