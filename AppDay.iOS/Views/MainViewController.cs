﻿using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using MvvmCross.Binding.BindingContext;
using AppDay.Core.ViewModels;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace AppDay.Core.iOS.Views
{
    [MvxRootPresentation(WrapInNavigationController = true)]
    public partial class MainViewController : MvxViewController
    {
        private UIDatePicker _picker;

        public MainViewController() : base("MainViewController", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            _picker = new UIDatePicker();
            _picker.Mode = UIDatePickerMode.DateAndTime;
            _dateTimeField.InputView = _picker;

            BindControls();
        }

        public void BindControls()
        {
            var set = this.CreateBindingSet<MainViewController, MainViewModel>();

            set.Bind(_fromTextField).To(vm => vm.FromStation);
            set.Bind(_toTextField).To(vm => vm.ToStation);
            set.Bind(_dateTimeField).To(vm => vm.DateTimeString);
            set.Bind(_picker).To(vm => vm.DateTime);

            set.Bind(_checkButton).To(vm => vm.CheckCommand);
            set.Bind(_applyButton).To(vm => vm.ApplyCommand);

            set.Apply();
        }
    }
}

