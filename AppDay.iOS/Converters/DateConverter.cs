﻿using System;
using System.Globalization;
using Foundation;
using MvvmCross.Platform.Converters;

namespace AppDay.Core.iOS.Converters
{
    public class DateConverter : MvxValueConverter<NSDate, DateTime>
    {
        protected override DateTime Convert(NSDate date, Type targetType, object parameter, CultureInfo culture)
        {
            return (new DateTime(2001, 1, 1, 0, 0, 0)).AddSeconds(date.SecondsSinceReferenceDate);
        }
        protected override NSDate ConvertBack(DateTime date, Type targetType, object parameter, CultureInfo culture)
        {
            return NSDate.FromTimeIntervalSinceReferenceDate((date - (new DateTime(2001, 1, 1, 0, 0, 0))).TotalSeconds);
        }
    }
}
