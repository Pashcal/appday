﻿using System;
namespace AppDay.API
{
    public static class ApiSettings
    {
        public static string MobileUrl { get { return "http://mobile.bahn.de/"; } }
        public static string GroupUrl { get { return "https://api.deutschebahn.com/"; } }
    }
}
