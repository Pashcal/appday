﻿using System;
using Newtonsoft.Json;

namespace AppDay.API.Models
{
    public class Ticket
    {
        [JsonProperty("from")]
        public string From { get; set; }

        [JsonProperty("to")]
        public string To { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }
    }
}
