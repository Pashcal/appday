﻿using System;
using Newtonsoft.Json;

namespace AppDay.API.Models
{
    public class SaveTicket
    {
        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("ticket")]
        public Ticket Ticket { get; set; }
    }
}
