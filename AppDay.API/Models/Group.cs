﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AppDay.API.Models
{
    public class Group
    {
        [JsonProperty("ticket")]
        public Ticket Ticket { get; set; }

        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("userIds")]
        public List<string> UserIds { get; set; }
    }
}
