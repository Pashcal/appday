﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using AppDay.API.Models;
namespace AppDay.API.Services
{
    public interface ITicketsService
    {
        Task<List<Ticket>> GetTickets(string from, string to, DateTime datetime);
    }
}
