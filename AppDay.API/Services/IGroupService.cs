﻿using System;
using System.Threading.Tasks;
using AppDay.API.Models;
namespace AppDay.API.Services
{
    public interface IGroupService
    {
        Task SaveTicket(SaveTicket saveTicket);

        Task<Group> CheckGroup(string userId);
    }
}
