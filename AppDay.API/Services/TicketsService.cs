﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AppDay.API.Models;
using Xablu.WebApiClient;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Extensions;
using System.Linq;
using System.Globalization;

namespace AppDay.API.Services
{
    public class TicketsService : ITicketsService
    {
        public async Task<List<Ticket>> GetTickets(string from, string to, DateTime datetime)
        {
            var query = $"{ApiSettings.MobileUrl}bin/mobil/query.exe/dox?S={from}&Z={to}" +
                $"&date={datetime:dd.MM.yy}&time={datetime:HH:mm}&start=1&REQ0JourneyProduct_opt0=1";

            var document = await BrowsingContext.New(Configuration.Default.WithDefaultLoader())
                                                .OpenAsync(query);
            return ParseTickets(document, from, to);
        }

        private List<Ticket> ParseTickets(IDocument document, string from, string to)
        {
            var tickets = new List<Ticket>();

            var times = document.QuerySelectorAll("tr.scheduledCon td:nth-child(1)");
            var prices = document.QuerySelectorAll("tr.scheduledCon td:nth-child(4)");
            for (var i = 0; i < times.Length; i++)
            {
                var time = string.Join(" - ", times[i].QuerySelectorAll("span").Select(x => x.InnerHtml));
                decimal price;
                decimal.TryParse(prices[i].QuerySelector("span").InnerHtml.Trim().Replace(',', '.'), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out price);
                tickets.Add(new Ticket
                {
                    From = from,
                    To = to,
                    Time = time,
                    Price = price
                });
            }

            return tickets;
        }
    }
}
