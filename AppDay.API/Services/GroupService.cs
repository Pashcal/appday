﻿using System;
using System.Threading.Tasks;
using AppDay.API.Models;
using Fusillade;
using Xablu.WebApiClient;
using System.Net.Http;
namespace AppDay.API.Services
{
    public class GroupService : BaseClient, IGroupService
    {
        public GroupService(IWebApiClient apiClient) : base(apiClient)
        {
        }

        public async Task<Group> CheckGroup(string userId)
        {
            var query = $"{ApiSettings.GroupUrl}group/check?userId={userId}";
            return await apiClient.GetAsync<Group>(Priority.UserInitiated, query);
        }

        public async Task SaveTicket(SaveTicket saveTicket)
        {
            await apiClient.PostAsync<SaveTicket, HttpResponseMessage>(Priority.UserInitiated,
                                                                       $"{ApiSettings.GroupUrl}group/save",
                                                                       saveTicket);
        }
    }
}
