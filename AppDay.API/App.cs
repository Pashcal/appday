﻿using System;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using AppDay.API.Services;
using Xablu.WebApiClient;

namespace AppDay.API
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            Mvx.LazyConstructAndRegisterSingleton<ITicketsService>(() =>new TicketsService());
            Mvx.LazyConstructAndRegisterSingleton<IGroupService>(() =>new GroupService(Mvx.Resolve<IWebApiClient>()));
        }
    }
}
